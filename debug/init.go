package debug

import (
	"io"
	"os"
)

var Output io.Writer

func init() {
	Output = os.Stdout
}
