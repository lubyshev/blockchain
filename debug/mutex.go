package debug

import (
	"sync"
)

//####################

type Mutex struct {
	sync.Mutex
}

var DumpMutexInfo bool

func (m *Mutex) Lock() {
	if DumpMutexInfo {
		dumpInfo("Mutex Lock")
	}
	m.Mutex.Lock()
}

func (m *Mutex) Unlock() {
	if DumpMutexInfo {
		dumpInfo("Mutex Unlock")
	}
	m.Mutex.Unlock()
}

//####################

type RWMutex struct {
	sync.RWMutex
}

var DumpRWMutexInfo bool

func (m *RWMutex) RLock() {
	if DumpRWMutexInfo {
		dumpInfo("RWMutex RLock")
	}
	m.RWMutex.RLock()
}

func (m *RWMutex) RUnlock() {
	if DumpRWMutexInfo {
		dumpInfo("RWMutex RUnlock")
	}
	m.RWMutex.RUnlock()
}

func (m *RWMutex) Lock() {
	if DumpRWMutexInfo {
		dumpInfo("RWMutex Lock")
	}
	m.RWMutex.Lock()
}

func (m *RWMutex) Unlock() {
	if DumpRWMutexInfo {
		dumpInfo("RWMutex Unlock")
	}
	m.RWMutex.Unlock()
}
