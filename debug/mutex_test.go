package debug_test

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/lubyshev/blockchain/debug"
)

func TestMutex(t *testing.T) {
	var mx debug.Mutex
	var str string
	buf := bytes.NewBufferString(str)
	debug.Output = buf
	debug.DumpMutexInfo = true
	mx.Lock()
	assert.Equal(t, "DEBUG: Mutex Lock gitlab.com/lubyshev/blockchain/debug_test.TestMutex:17\n", buf.String())
	buf.Reset()
	mx.Unlock()
	assert.Equal(t, "DEBUG: Mutex Unlock gitlab.com/lubyshev/blockchain/debug_test.TestMutex:20\n", buf.String())
}

func TestRWMutex(t *testing.T) {
	var mx debug.RWMutex
	var str string
	buf := bytes.NewBufferString(str)
	debug.Output = buf
	debug.DumpRWMutexInfo = true
	mx.Lock()
	assert.Equal(t, "DEBUG: RWMutex Lock gitlab.com/lubyshev/blockchain/debug_test.TestRWMutex:30\n", buf.String())
	buf.Reset()
	mx.Unlock()
	assert.Equal(t, "DEBUG: RWMutex Unlock gitlab.com/lubyshev/blockchain/debug_test.TestRWMutex:33\n", buf.String())
	buf.Reset()
	mx.RLock()
	assert.Equal(t, "DEBUG: RWMutex RLock gitlab.com/lubyshev/blockchain/debug_test.TestRWMutex:36\n", buf.String())
	buf.Reset()
	mx.RUnlock()
	assert.Equal(t, "DEBUG: RWMutex RUnlock gitlab.com/lubyshev/blockchain/debug_test.TestRWMutex:39\n", buf.String())
}
