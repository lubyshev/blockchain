package tests

import (
	"context"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/lubyshev/blockchain/block"
	"gitlab.com/lubyshev/blockchain/chain"
	"gitlab.com/lubyshev/blockchain/mining"
	"math/rand"
	"testing"
	"time"
)

func TestBlockSerialize(t *testing.T) {
	idSrc := ksuid.New()
	idDst := ksuid.New()
	ctx := context.Background()
	ch := chain.Genesis(ctx, mining.NewMiningFactory(time.Minute, validator), chain.NewBalance(), nil)
	err := <-ch.AddNewBlock(ksuid.Max, idSrc, uint64(10000))
	assert.NoError(t, err)
	err = <-ch.AddNewBlock(idSrc, idDst, uint64(rand.Intn(10000)))
	assert.NoError(t, err)

	blockOne, _ := ch.GetBlock(1)
	data := block.ToBinary(blockOne)
	blockTwo := block.FromBinary(data)

	assert.Equal(t, block.ChainBlockSize, len(data))
	assert.Equal(t, *blockOne, blockTwo)
}
