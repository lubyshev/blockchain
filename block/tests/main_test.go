package tests

import (
	"gitlab.com/lubyshev/blockchain/block"
)

const validHashZeros = 2

var validator block.HashValidator = func(hash *block.ChainBlockHash) bool {
	for _, b := range hash[:validHashZeros] {
		if b != 0 {
			return false
		}
	}

	return true
}
