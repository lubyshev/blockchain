package block

import (
	"crypto/sha256"
	"github.com/segmentio/ksuid"
)

const ChainBlockSize = 160

type ChainBlockData [ChainBlockSize]byte
type ChainBlockHash [sha256.BlockSize / 2]byte
type HashValidator func(hash *ChainBlockHash) bool

type Block struct {
	Id        ksuid.KSUID    // id of block
	From      ksuid.KSUID    // WalletId of transaction donor
	To        ksuid.KSUID    // WalletId of transaction recipient
	Amount    uint64         // Amount of the transaction
	Proof     uint64         // Proof of the mining
	PrevId    ksuid.KSUID    // Previous block id
	PrevHash  ChainBlockHash // Previous block hash
	Signature ChainBlockHash // Block signature
}

var nullChainBlockHash = ChainBlockHash{
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
}

func NullChainBlockHash() ChainBlockHash {
	return nullChainBlockHash
}

func ToBinary(b *Block) (binary *ChainBlockData) {
	idx := 0
	binary = new(ChainBlockData)
	for _, b := range b.Id {
		binary[idx] = b
		idx++
	}
	for _, b := range b.From {
		binary[idx] = b
		idx++
	}
	for _, b := range b.To {
		binary[idx] = b
		idx++
	}
	for i := 0; i < 8; i++ {
		binary[idx] = byte(b.Amount >> (56 - i*8))
		idx++
	}
	for _, b := range b.PrevId {
		binary[idx] = b
		idx++
	}
	for i := 0; i < 8; i++ {
		binary[idx] = byte(b.Proof >> (56 - i*8))
		idx++
	}
	for _, b := range b.PrevHash {
		binary[idx] = b
		idx++
	}
	for _, b := range b.Signature {
		binary[idx] = b
		idx++
	}

	return
}

func FromBinary(binary *ChainBlockData) (b Block) {
	idx := 0
	for i := 0; i < len(b.Id); i++ {
		b.Id[i] = binary[idx]
		idx++
	}
	for i := 0; i < len(b.From); i++ {
		b.From[i] = binary[idx]
		idx++
	}
	for i := 0; i < len(b.To); i++ {
		b.To[i] = binary[idx]
		idx++
	}
	for i := 0; i < 8; i++ {
		b.Amount += uint64(binary[idx]) << (56 - i*8)
		idx++
	}
	for i := 0; i < len(b.PrevId); i++ {
		b.PrevId[i] = binary[idx]
		idx++
	}
	for i := 0; i < 8; i++ {
		b.Proof += uint64(binary[idx]) << (56 - i*8)
		idx++
	}
	for i := 0; i < len(b.PrevHash); i++ {
		b.PrevHash[i] = binary[idx]
		idx++
	}
	for i := 0; i < len(b.Signature); i++ {
		b.Signature[i] = binary[idx]
		idx++
	}

	return
}
