package mining

import (
	"context"
	"crypto/sha256"
	"gitlab.com/lubyshev/blockchain/block"
	"time"
)

func mineBlock(ctx context.Context, duration time.Duration, b *block.Block, validate block.HashValidator, ch chan<- block.ChainBlockHash) {
	defer close(ch)

	var null = block.NullChainBlockHash()
	var ctxMine, cf = context.WithTimeout(ctx, duration)
	defer cf()

	for {
		select {
		case <-ctxMine.Done():
			ch <- null
			return
		default:
			if res := blockHash(b); res != null && validate(&res) {
				ch <- res
				return
			}
			b.Proof++
		}
	}
}

func signBlock(ctx context.Context, b *block.Block, ch chan<- block.ChainBlockHash) {
	defer close(ch)

	select {
	case <-ctx.Done():
		ch <- block.NullChainBlockHash()
	default:
		ch <- blockHash(b)
	}
}

func blockHash(b *block.Block) (res block.ChainBlockHash) {
	hash := sha256.New()
	src := block.ToBinary(b)
	hash.Write(src[:])
	copy(res[:], hash.Sum(nil)[:])

	return
}
