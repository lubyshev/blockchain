package mining

import (
	"context"
	"gitlab.com/lubyshev/blockchain/block"
	"time"
)

type Factory interface {
	// Mine New block hash mining.
	//
	// @param ctx context.Context Mining context.
	//
	// @param  b *block.Block Block to mine.
	//
	// @return <-chan block.ChainBlockHash Calculated hash.
	//
	// When timeout (mineFactory.miningDuration) occurred or context is done
	// then the value of block.NullChainBlockHash() will be placed in the returned channel.
	Mine(ctx context.Context, b *block.Block) <-chan block.ChainBlockHash

	// Sign Returns block signature.
	//
	// @param ctx context.Context Signing context.
	//
	// @param  b *block.Block Block to sign.
	//
	// @return <-chan block.ChainBlockHash Calculated hash.
	//
	// When context is done then the value of block.NullChainBlockHash()
	// will be placed in the returned channel.
	Sign(ctx context.Context, b *block.Block) <-chan block.ChainBlockHash

	// Validate Is block hash is equal hash param.
	//
	//
	// @param block.Block Validated block.
	//
	// @param  block.ChainBlockHash Hash.
	//
	// @return bool Returns true when equal.
	Validate(b *block.Block, hash block.ChainBlockHash) bool
}

func NewMiningFactory(miningDuration time.Duration, validator block.HashValidator) Factory {
	return &mineFactory{miningDuration: miningDuration, validator: validator}
}

type mineFactory struct {
	miningDuration time.Duration
	validator      block.HashValidator
}

func (m *mineFactory) Mine(ctx context.Context, b *block.Block) <-chan block.ChainBlockHash {
	var ch = make(chan block.ChainBlockHash)

	go mineBlock(ctx, m.miningDuration, b, m.validator, ch)

	return ch
}

func (m *mineFactory) Sign(ctx context.Context, b *block.Block) <-chan block.ChainBlockHash {
	var ch = make(chan block.ChainBlockHash)

	go signBlock(ctx, b, ch)

	return ch
}

func (m *mineFactory) Validate(b *block.Block, hash block.ChainBlockHash) bool {
	return blockHash(b) == hash
}
