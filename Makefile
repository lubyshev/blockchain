lint:
	golangci-lint run ./... -v

cover:
	@go clean -testcache
	@go test $(go list ./...) -v -covermode=count -coverprofile=coverage.out -coverpkg=./...
	@goveralls -coverprofile=coverage.out -service=travis-ci -repotoken=${BLOCKCHAIN_COVERALL_TOKEN}
