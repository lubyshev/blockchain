package chain

import (
	"fmt"
	"github.com/segmentio/ksuid"
	"sync"
)

var (
	ErrAccountNotExists   = fmt.Errorf("account does not exists")
	ErrYourselfFounds     = fmt.Errorf("transfer of funds to yourself")
	ErrInsufficientFounds = fmt.Errorf("insufficient funds")
	ErrZeroAmount         = fmt.Errorf("transfer of zero amount")
)

type AccountingEntries map[ksuid.KSUID]uint64

type Balance interface {
	AddEntry(debit, credit ksuid.KSUID, amount uint64) error
	Balance(id ksuid.KSUID) (uint64, error)
}

func NewBalance() Balance {
	return &balance{entries: make(AccountingEntries)}
}

type balance struct {
	entries AccountingEntries
	mxEntry sync.Mutex
}

func (b *balance) AddEntry(debit, credit ksuid.KSUID, amount uint64) error {
	if debit == credit {
		if debit == ksuid.Max {
			return nil
		}
		return ErrYourselfFounds
	}
	if amount == 0 {
		return ErrZeroAmount
	}

	b.mxEntry.Lock()
	defer b.mxEntry.Unlock()

	if debit != ksuid.Max {
		if a, ok := b.entries[debit]; !ok || float64(a)-float64(amount) < 0 {
			return ErrInsufficientFounds
		}
		b.entries[debit] -= amount
		if b.entries[debit] == 0 {
			delete(b.entries, debit)
		}
	}
	if credit != ksuid.Max {
		b.entries[credit] += amount
	}

	return nil
}

func (b *balance) Balance(id ksuid.KSUID) (uint64, error) {
	defer b.mxEntry.Unlock()
	b.mxEntry.Lock()

	if a, ok := b.entries[id]; !ok {
		return 0, ErrAccountNotExists
	} else {
		return a, nil
	}
}
