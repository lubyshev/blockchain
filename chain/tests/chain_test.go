package tests

import (
	"context"
	"fmt"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/lubyshev/blockchain/block"
	"gitlab.com/lubyshev/blockchain/chain"
	"gitlab.com/lubyshev/blockchain/mining"
	"sync"
	"testing"
	"time"
)

func TestChain(t *testing.T) {
	const firstBlockAmount = uint64(5)
	var err error

	idSrc := ksuid.New()
	idDst := ksuid.New()

	ctx, cancel := context.WithCancel(context.Background())
	ch := chain.Genesis(ctx, factory, chain.NewBalance(), nil)

	<-ch.AddNewBlock(ksuid.Max, idSrc, firstBlockAmount)
	err = <-ch.AddNewBlock(idSrc, idDst, 10)
	assert.Equal(t, chain.ErrInsufficientFounds, err)

	t.Run("OK", func(t *testing.T) {
		tm := time.Now()
		for i := 0; i < testAddBlockCount; i++ {
			<-ch.AddNewBlock(idSrc, idDst, 1)
		}
		assert.Greater(t, time.Since(tm), testAddBlockDuration)

		tm = time.Now()
		n, err := ch.Validate()
		assert.Less(t, time.Since(tm), testValidateDuration)
		assert.NoError(t, err)
		assert.Equal(t, ch.BlocksCount(), n)
	})

	t.Run("FAIL: invalid block number", func(t *testing.T) {
		var index = ch.BlocksCount()
		_, err := ch.GetBlock(index)
		assert.Error(t, err)
		assert.Equal(t, fmt.Sprintf("index out of range: %d (last=%d)", index, index-1), err.Error())
	})

	t.Run("FAIL: queue limit", func(t *testing.T) {
		var old int32
		old, chain.QueueLimit = chain.QueueLimit, 0
		var ch = ch.AddNewBlock(ksuid.New(), ksuid.New(), 10)
		assert.Equal(t, (<-chan error)(nil), ch)
		chain.QueueLimit = old
	})

	t.Run("FAIL: replaced block", func(t *testing.T) {
		b, err := ch.GetBlock(1)
		assert.NoError(t, err)
		assert.Equal(t, firstBlockAmount, b.Amount)
		b.Amount = 100_000
		n, err := ch.Validate()
		assert.Error(t, err)
		assert.Equal(t, "fail to validate chain: block 1 has invalid signature", err.Error())
		assert.Equal(t, 1, n)
		// restore
		b.Amount = firstBlockAmount
	})

	/*
	 * test panics
	 */

	var wg sync.WaitGroup
	var mxGenesis sync.Mutex // prevent the use of blockchain.NullUuid along with other panic assertions

	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer func() {
			if r := recover(); r != nil {
				if err, ok := r.(error); ok {
					t.Run("PANIC: nil mining factory", func(t *testing.T) {
						assert.Equal(t, "invalid miner: nil", err.Error())
					})
				} else {
					panic(fmt.Errorf("invalid recovered panic type: expected 'error' got '%T'", r))
				}
			}
			wg.Done()
		}()

		mxGenesis.Lock()
		defer mxGenesis.Unlock()
		ctx := context.Background()
		_ = chain.Genesis(ctx, nil, chain.NewBalance(), nil)
	}(&wg)

	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer func() {
			if r := recover(); r != nil {
				if err, ok := r.(error); ok {
					t.Run("PANIC: nil balance", func(t *testing.T) {
						assert.Equal(t, "invalid balance: nil", err.Error())
					})
				} else {
					panic(fmt.Errorf("invalid recovered panic type: expected 'error' got '%T'", r))
				}
			}
			wg.Done()
		}()

		mxGenesis.Lock()
		defer mxGenesis.Unlock()
		ctx := context.Background()
		_ = chain.Genesis(ctx, factory, nil, nil)
	}(&wg)

	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer func() {
			if r := recover(); r != nil {
				if err, ok := r.(error); ok {
					t.Run("PANIC: genesis timeout", func(t *testing.T) {
						assert.Equal(t, "fail to calculate base chain block: timeout", err.Error())
					})
				} else {
					panic(fmt.Errorf("invalid recovered panic type: expected 'error' got '%T'", r))
				}
			}
			wg.Done()
		}()

		// prevent the use of blockchain.NullUuid along with other panic assertions
		mxGenesis.Lock()
		defer mxGenesis.Unlock()

		validHashZeros = 5
		ctx := context.Background()
		_ = chain.Genesis(ctx, factory, chain.NewBalance(), nil)
		validHashZeros = 2
	}(&wg)

	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer func() {
			if r := recover(); r != nil {
				if err, ok := r.(error); ok {
					t.Run("PANIC: genesis InsufficientFounds", func(t *testing.T) {
						assert.Equal(t, chain.ErrInsufficientFounds, err)
					})
				} else {
					panic(fmt.Errorf("invalid recovered panic type: expected 'error' got '%T'", r))
				}
			}
			wg.Done()
		}()

		ctx := context.Background()
		mxGenesis.Lock()
		defer mxGenesis.Unlock()

		_ = chain.Genesis(ctx, factory, chain.NewBalance(), []block.Block{
			{Id: ksuid.New(), From: ksuid.Max, To: ksuid.Max},
			{Id: ksuid.New(), From: ksuid.New(), To: ksuid.New(), Amount: 100},
		})
	}(&wg)

	wg.Wait()

	validHashZeros = 1
	ch2 := chain.Genesis(ctx, mining.NewMiningFactory(10*time.Millisecond, validator), chain.NewBalance(), nil)
	err = <-ch2.AddNewBlock(ksuid.Max, idSrc, 50)
	assert.NoError(t, err)

	validHashZeros = 2
	t.Run("FAIL: add block: timeout", func(t *testing.T) {
		var err = <-ch2.AddNewBlock(idSrc, idDst, 10)
		assert.NoError(t, err)
	})

	t.Run("OK: data integrity", func(t *testing.T) {
		n, err := ch.CheckDataIntegrity()
		assert.NoError(t, err)
		assert.Equal(t, ch.BlocksCount(), n)
	})

	cancel()

	t.Run("FAIL: add block: context is done", func(t *testing.T) {
		var err = <-ch.AddNewBlock(idSrc, idDst, 10)
		assert.Equal(t, chain.ErrContextIsDone, err)
	})

	t.Run("FAIL: validate: context is done", func(t *testing.T) {
		_, err := ch.Validate()
		assert.Equal(t, chain.ErrContextIsDone, err)
	})

	t.Run("FAIL: data integrity: context is done", func(t *testing.T) {
		_, err := ch.CheckDataIntegrity()
		assert.Equal(t, chain.ErrContextIsDone, err)
	})
}
