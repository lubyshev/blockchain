package tests

import (
	"gitlab.com/lubyshev/blockchain/block"
	"gitlab.com/lubyshev/blockchain/mining"
	"time"
)

const testAddBlockCount = 10
const testAddBlockDuration = 100 * time.Millisecond
const testValidateDuration = 500 * time.Microsecond

var validHashZeros = 1
var factory = mining.NewMiningFactory(10*time.Second, validator)

var validator block.HashValidator = func(hash *block.ChainBlockHash) bool {
	for _, b := range hash[:validHashZeros] {
		if b != 0 {
			return false
		}
	}

	return true
}
