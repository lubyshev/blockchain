package tests

import (
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/lubyshev/blockchain/chain"
	"testing"
)

func TestBalance(t *testing.T) {
	b := chain.NewBalance()
	idSrc := ksuid.New()
	idDst := ksuid.New()

	err := b.AddEntry(idSrc, idSrc, 10)
	assert.Error(t, err)
	assert.Equal(t, chain.ErrYourselfFounds, err)

	err = b.AddEntry(idSrc, idDst, 10)
	assert.Error(t, err)
	assert.Equal(t, chain.ErrInsufficientFounds, err)

	_, err = b.Balance(idSrc)
	assert.Error(t, err)
	assert.Equal(t, chain.ErrAccountNotExists, err)

	err = b.AddEntry(ksuid.Max, ksuid.Max, 10)
	assert.NoError(t, err)

	err = b.AddEntry(ksuid.Max, idSrc, 0)
	assert.Error(t, err)
	assert.Equal(t, chain.ErrZeroAmount, err)

	err = b.AddEntry(ksuid.Max, idSrc, 10)
	assert.NoError(t, err)
	a, err := b.Balance(idSrc)
	assert.NoError(t, err)
	assert.Equal(t, uint64(10), a)

	err = b.AddEntry(idSrc, idDst, 20)
	assert.Error(t, err)
	assert.Equal(t, chain.ErrInsufficientFounds, err)

	err = b.AddEntry(idSrc, idDst, 5)
	assert.NoError(t, err)
	a, err = b.Balance(idSrc)
	assert.NoError(t, err)
	assert.Equal(t, uint64(5), a)
	a, err = b.Balance(idDst)
	assert.NoError(t, err)
	assert.Equal(t, uint64(5), a)

	err = b.AddEntry(idSrc, idDst, 5)
	assert.NoError(t, err)
	_, err = b.Balance(idSrc)
	assert.Error(t, err)
	assert.Equal(t, chain.ErrAccountNotExists, err)
	a, err = b.Balance(idDst)
	assert.NoError(t, err)
	assert.Equal(t, uint64(10), a)
}
