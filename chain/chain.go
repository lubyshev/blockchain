package chain

import (
	"context"
	"fmt"
	"github.com/segmentio/ksuid"
	"gitlab.com/lubyshev/blockchain/block"
	"gitlab.com/lubyshev/blockchain/mining"
	"sync"
	"sync/atomic"
	"time"
)

var QueueLimit = int32(256)

var ErrContextIsDone = fmt.Errorf("contex is done")
var ErrEmptyQueue = fmt.Errorf("block queue is empty")

type Chain interface {
	AddNewBlock(src, dst ksuid.KSUID, amount uint64) (ready <-chan error)
	BlocksCount() int
	CheckDataIntegrity() (int, error)
	GetBlock(n int) (*block.Block, error)
	Validate() (int, error)
}

// Genesis Create the root chain block instance.
//
// @param ctx context.Context Blockchain context.
//
// @param miner mining.Factory Mining factory.
//
// @param balance Balance Finance factory.
//
// @param readyChain []block.Block Initial chain or nil to generate from scratch.
func Genesis(ctx context.Context, miner mining.Factory, balance Balance, readyChain []block.Block) Chain {
	// if context not present
	if ctx == nil {
		panic(fmt.Errorf("invalid context: nil"))
	}
	// if mining factory not present
	if miner == nil {
		panic(fmt.Errorf("invalid miner: nil"))
	}
	// if balance not present
	if balance == nil {
		panic(fmt.Errorf("invalid balance: nil"))
	}

	// calculate block hash
	var b block.Block
	if readyChain == nil {
		// genesis from scratch
		b = block.Block{Id: ksuid.New(), From: ksuid.Max, To: ksuid.Max}
		b.Signature = <-miner.Sign(ctx, &b)
		if b.Signature == block.NullChainBlockHash() {
			panic(fmt.Errorf("fail to calculate base chain signature: timeout"))
		}
		readyChain = []block.Block{b}
	}

	for _, b := range readyChain {
		if err := balance.AddEntry(b.From, b.To, b.Amount); err != nil {
			panic(err)
		}
	}

	return &chain{
		ctx:     ctx,
		chain:   readyChain,
		miner:   miner,
		balance: balance,
		lastId:  readyChain[len(readyChain)-1].Id,
		queue:   make([]ksuid.KSUID, 0),
	}
}

type chain struct {
	ctx context.Context

	chain   []block.Block
	mxChain sync.RWMutex

	lastId      ksuid.KSUID
	lastHash    block.ChainBlockHash
	mxLastBlock sync.RWMutex

	miner     mining.Factory
	refsQueue int32

	balance Balance

	queue   []ksuid.KSUID
	mxQueue sync.Mutex
}

func (c *chain) AddNewBlock(src, dst ksuid.KSUID, amount uint64) (ready <-chan error) {
	if c.getRefQueue() >= QueueLimit {
		return nil
	}
	c.addRefQueue()

	b := block.Block{Id: ksuid.New(), From: src, To: dst, Amount: amount}
	b.PrevId = c.getLastId(b.Id)

	// send mining request to factory
	ch := make(chan error)
	go c.mineBlock(&b, ch)

	return ch
}

func (c *chain) BlocksCount() int {
	defer c.mxChain.RUnlock()
	c.mxChain.RLock()

	return len(c.chain)
}

func (c *chain) GetBlock(n int) (*block.Block, error) {
	defer c.mxChain.RUnlock()
	c.mxChain.RLock()

	l := c.BlocksCount() - 1
	if n < 0 || n > l {
		return nil, fmt.Errorf("index out of range: %d (last=%d)", n, l)
	}

	return &c.chain[n], nil
}

func (c *chain) CheckDataIntegrity() (int, error) {
	var err error
	l := c.BlocksCount()
	for i := 0; i < l-1; i++ {
		select {
		case <-c.ctx.Done():
			return 0, ErrContextIsDone
		default:
			c.mxChain.RLock()
			if c.chain[i].Id != c.chain[i+1].PrevId {
				err = fmt.Errorf("block %d is not approved in the next block", i)
			}
			c.mxChain.RUnlock()
			if err != nil {
				return i, err
			}
		}
	}

	return l, nil
}

func (c *chain) Validate() (int, error) {
	l := c.BlocksCount()
	for i := 0; i < l-1; i++ {
		select {
		case <-c.ctx.Done():
			return 0, ErrContextIsDone
		default:
			var sign block.ChainBlockHash
			c.mxChain.RLock()
			b := c.chain[i]
			c.mxChain.RUnlock()
			sign, b.Signature = b.Signature, block.ChainBlockHash{}
			if !c.miner.Validate(&b, sign) {
				return i, fmt.Errorf("fail to validate chain: block %d has invalid signature", i)
			}
		}
	}

	return l, nil
}

func (c *chain) getRefQueue() int32 {
	return atomic.LoadInt32(&c.refsQueue)
}

func (c *chain) addRefQueue() {
	atomic.AddInt32(&c.refsQueue, 1)
}

func (c *chain) releaseRefQueue() {
	atomic.AddInt32(&c.refsQueue, -1)
}

func (c *chain) getLastId(nextLastId ksuid.KSUID) ksuid.KSUID {
	defer c.mxQueue.Unlock()
	defer c.mxLastBlock.RUnlock()
	c.mxQueue.Lock()
	c.mxLastBlock.RLock()

	res := c.lastId
	if l := len(c.queue); l > 0 {
		res = c.queue[l-1]
	}
	c.queue = append(c.queue, nextLastId)

	return res
}

func (c *chain) mineBlock(b *block.Block, ready chan<- error) {
	defer func() {
		c.releaseRefQueue()
	}()

	for {
		select {
		case <-c.ctx.Done():
			ready <- ErrContextIsDone
			close(ready)
			return
		case hash := <-c.miner.Mine(c.ctx, b):
			// repeat if mining timeout occurred
			if hash == block.NullChainBlockHash() {
				continue
			}
			go c.waitForSignature(b, hash, ready)
			return
		}
	}
}

func (c *chain) waitForSignature(b *block.Block, hash block.ChainBlockHash, ready chan<- error) {
	defer close(ready)
	var tick = time.NewTicker(100 * time.Millisecond)

	for {
		select {
		case <-c.ctx.Done():
			ready <- ErrContextIsDone
			return

		case <-tick.C:
			var err error
			var approved bool
			tick.Stop()

			c.mxQueue.Lock()
			if len(c.queue) == 0 {
				err = ErrEmptyQueue
			} else {
				if c.queue[0] == b.Id {
					approved = true
				}
			}
			c.mxQueue.Unlock()

			if err != nil {
				ready <- err
				return
			}

			if approved {
				c.mxChain.Lock()
				c.mxQueue.Lock()
				b.PrevHash = c.lastHash
				b.Signature = <-c.miner.Sign(c.ctx, b)
				// if context is done then return
				if b.Signature == block.NullChainBlockHash() {
					err = ErrContextIsDone
				} else {
					if err = c.balance.AddEntry(b.From, b.To, b.Amount); err == nil {
						c.mxLastBlock.Lock()
						c.lastId = b.Id
						c.lastHash = hash
						c.chain = append(c.chain, *b)
						c.mxLastBlock.Unlock()
					}
				}
				c.queue = c.queue[1:]
				c.mxQueue.Unlock()
				c.mxChain.Unlock()
				ready <- err
				return
			}

			tick.Reset(100 * time.Millisecond)
		}
	}
}
