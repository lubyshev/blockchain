package benchmarks_test

import (
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/assert"
	"math/rand"
	"sync"
	"testing"
	"time"
)

func BenchmarkConcurrent(b *testing.B) {
	var wg sync.WaitGroup
	b.Log("Start with Blocks: ", ch.BlocksCount())

	b.RunParallel(func(b *testing.PB) {
		for b.Next() {
			id1 := ksuid.New()
			id2 := ksuid.New()
			amount := uint64(rand.Intn(1000)) + 10

			for attempts := 100; attempts > 0; attempts-- {
				runTransaction(&wg, ksuid.Max, id1, amount)
				runTransaction(&wg, id1, id2, amount/2)
				time.Sleep(time.Millisecond)
			}
		}
	})
	wg.Wait()

	b.Log("Finish with Blocks:", ch.BlocksCount())
	n, err := ch.CheckDataIntegrity()
	assert.NoError(b, err)
	assert.Equal(b, ch.BlocksCount(), n)
}

func runTransaction(wg *sync.WaitGroup, from, to ksuid.KSUID, amount uint64) {
	var r <-chan error
	var tickDuration = 100 * time.Millisecond
	var tick = time.NewTicker(tickDuration)
	for {
		if r = ch.AddNewBlock(from, to, amount); r != nil {
			break
		}
		select {
		case <-ctxMain.Done():
			return
		case <-tick.C:
			tick.Reset(tickDuration)
		}
	}
	wg.Add(1)
	go func(wg *sync.WaitGroup, ready <-chan error) {
		defer wg.Done()
		<-ready
	}(wg, r)
}
