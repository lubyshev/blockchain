package benchmarks_test

import (
	"bufio"
	"context"
	"errors"
	"gitlab.com/lubyshev/blockchain/block"
	bc "gitlab.com/lubyshev/blockchain/chain"
	"gitlab.com/lubyshev/blockchain/mining"
	"os"
	"testing"
	"time"
)

const validHashZeros = 2
const chainFile = "./metrics/chain.bch"
const BlocksToSave = 50 // 50 * 160 = 8000 ≈ page size in memory

var validator block.HashValidator = func(hash *block.ChainBlockHash) bool {
	for _, b := range hash[:validHashZeros] {
		if b != 0 {
			return false
		}
	}

	return true
}

var ch bc.Chain
var ctxMain context.Context
var factory = mining.NewMiningFactory(time.Minute, validator)

func TestMain(m *testing.M) {
	var cancel context.CancelFunc
	ctxMain, cancel = context.WithCancel(context.Background())
	f, err := os.Stat(chainFile)
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		panic(err)
	}
	if f != nil && f.Size() > 0 {
		if ch, err = readChain(ctxMain, factory); err != nil {
			panic(err)
		}
	} else {
		ch = bc.Genesis(ctxMain, factory, bc.NewBalance(), nil)
	}
	if _, err = ch.Validate(); err != nil {
		panic(err)
	}
	exitCode := m.Run()
	for n, err := 0, error(nil); n != ch.BlocksCount(); n, err = ch.Validate() {
		if err == bc.ErrContextIsDone {
			break
		}
		if err != nil {
			panic(err)
		}
	}
	cancel()

	if err = saveChain(); err != nil {
		panic(err)
	}

	os.Exit(exitCode)
}

func saveChain() error {
	f, err := os.Create(chainFile)
	if err != nil {
		return err
	}
	defer func() {
		_ = f.Close()
	}()

	var chainBytes = make([]byte, 0, block.ChainBlockSize*BlocksToSave)
	for i := 0; i < ch.BlocksCount(); i++ {
		if i > 0 && i%BlocksToSave == 0 {
			if _, err = f.Write(chainBytes); err != nil {
				return err
			}
			chainBytes = chainBytes[:0]
		}
		var b *block.Block
		if b, err = ch.GetBlock(i); err != nil {
			return err
		}
		chainBytes = append(chainBytes, block.ToBinary(b)[:]...)
	}
	if len(chainBytes) > 0 {
		if _, err = f.Write(chainBytes); err != nil {
			return err
		}
	}

	return nil
}

func readChain(ctx context.Context, factory mining.Factory) (bc.Chain, error) {
	f, err := os.Open(chainFile)
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = f.Close()
	}()

	var blocks []block.Block
	if fi, _ := f.Stat(); fi.Size()%block.ChainBlockSize != 0 {
		return nil, errors.New("invalid blockchain file size")
	} else {
		blocks = make([]block.Block, 0, fi.Size()/block.ChainBlockSize)
	}

	var chainBytes = make([]byte, block.ChainBlockSize*BlocksToSave)
	var sl block.ChainBlockData

	r := bufio.NewReader(f)
	for {
		n, err := r.Read(chainBytes)
		if n == 0 {
			break
		}
		if err != nil {
			return nil, err
		}
		for idx := 0; idx < n/block.ChainBlockSize; idx++ {
			copy(sl[:], chainBytes[idx*block.ChainBlockSize:(idx+1)*block.ChainBlockSize])
			blocks = append(blocks, block.FromBinary(&sl))
		}
	}

	return bc.Genesis(ctx, factory, bc.NewBalance(), blocks), nil
}
