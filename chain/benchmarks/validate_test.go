package benchmarks_test

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestValidate(t *testing.T) {
	n, err := ch.Validate()
	assert.NoError(t, err)
	assert.Equal(t, ch.BlocksCount(), n)
	n, err = ch.CheckDataIntegrity()
	assert.NoError(t, err)
	assert.Equal(t, ch.BlocksCount(), n)
}
